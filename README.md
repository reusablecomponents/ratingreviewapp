RatingReviewSample works on Swift4.0 and requires ARC to build. It depends on the following Apple frameworks, which should already be included with most Xcode templates:

Foundation.framework
UIKit.framework

This sample provide you the easy and reusable way to write review and provide rating.


Usage:
pod 'Cosmos'

Use these lines in your code:

@IBOutlet weak var ratingView: CosmosView!

ratingView.rating = 0
ratingView.didTouchCosmos = { rating in
self.ratingLbl.text = String(format: "%d/5", Int(rating))
}
