//
//  RatingReviewController.swift
//  RatingReviewSample
//
//  Created by Pooja Rana on 27/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import Cosmos

class RatingReviewController: UIViewController {

    // MARK: -  UIButton 
    @IBOutlet weak var reviewTxt: CustomAnimatableTextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLbl: UILabel!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ratingView.rating = 0
        ratingView.didTouchCosmos = { rating in
            self.ratingLbl.text = String(format: "%d/5", Int(rating))
        }
    }

    // MARK: -  UIButton Action 
    @IBAction func submitAction(_ sender: Any) {
        let (msg, isclear) = reviewTxt.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 ? ("Please enter review first.", false) : (ratingView.rating == 0 ? ("Please provide rating.", false) : ("Thank you for submitting your valuable rating and reviews.", true))
        showAlert(msg: msg, isClear: isclear)
    }
    
    func showAlert(msg: String, isClear: Bool)  {
        let alert = UIAlertController(title: "RatingReviewSample", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: {
            if isClear {
                self.reviewTxt.text    = ""
                self.ratingView.rating = 0
            }
        })
    }
}
