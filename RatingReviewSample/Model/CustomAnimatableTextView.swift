//
//  CustomAnimatableTextfield.swift
//  UnderXS
//
//  Created by Pooja Rana on 31/08/17.
//  Copyright © 2017 Pooja Rana. All rights reserved.
//

import UIKit

let placeHolderArr = ["1001": "Description (required)",
                      "1002": "Biographical Info",
                      "1003": "Your Message",
                      "1004": "Enter Your Reason",
                      "1005": "Your message here"]

class CustomAnimatableTextView: UITextView, UITextViewDelegate {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
    }
    
    @IBInspectable internal var maxLength: Int = 0
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolderArr[String(textView.tag)] {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: [" ", "\n"]) == "" {
            textView.text = placeHolderArr[String(textView.tag)]
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentCharacterCount = textView.text?.count ?? 0
        let newLength = currentCharacterCount + text.count - range.length
        return maxLength != 0 ? newLength <= maxLength : true
    }
}
